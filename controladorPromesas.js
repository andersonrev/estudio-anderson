const {sumaPromesa,restaPromesa} = require('./OperacionesPromesas');

(async ()=>{
    try{
    const resultado = await sumaPromesa(5,4);
    console.log(resultado);
    const resultadoResta = await restaPromesa(5,4);
    console.log(resultadoResta);
    }catch (e) {
        console.log(e);
    }
})();