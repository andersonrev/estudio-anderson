const prompts = require('prompts');
const {of,from} = require('rxjs');
const {map,mergeMap} = require ('rxjs/operators');
const {operacion, sumaCB, resta, divicion, multiplicacion} = require('./funcionesCallBack');
const {sumaPromesa, restaPromesa} = require('./OperacionesPromesas');
//const {suma$} = require('./operaciones-observable')

const preguntaFormaSolucion = [
    {
        type: 'select',
        name: 'value',
        message: 'Elija una opción',
        choices: [
            {title: 'Operaciones Con CallBack', value: 'callBack'},
            {title: 'Operaciones Con Promesas', value: 'promesas'},
            {title: 'Operaciones Con Observables', value: 'observables'},
            {title: 'Operaciones Multiples', value: 'multiples'},
            {title: 'Salir', value: 's'}
        ],
        initial: 0
    }
];

const preguntaOperacion = [
    {
        type: 'select',
        name: 'value',
        message: 'Elija una opción',
        choices: [
            {title: 'Suma', value: 'suma'},
            {title: 'Resta', value: 'resta'},
            {title: 'Multiplicacion', value: 'multiplicacion'},
            {title: 'División', value: 'division'},
            {title: 'Salir', value: 's'}
        ],
        initial: 0
    }
];

const preguntaIngresoNumero = [
    {
        type: 'number',
        name: 'numero1',
        message: 'Ingrese el numero 1'
    },
    {
        type: 'number',
        name: 'numero2',
        message: 'Ingrese el numero 2'
    }
];

const desicionContinuar = [
    {
        type: 'toggle',
        name: 'continuar',
        message: 'Continuar?',
        initial: true,
        active: 'yes',
        inactive: 'no'
    }
];

function formatearRespuesta(resultado) {
    if (Number.isNaN(resultado)) {
        return {
            mensaje: `Parámetros inválidos`,
            valor: resultado
        }
    } else {
        return {
            mensaje: `El resultado requerido es: ${resultado}`,
            valor: resultado
        }
    }
}

function prepararRespuesta(observable$) {
    observable$
        .subscribe(respuesta => console.log(respuesta),
            error => console.log('Error:', error),
            fin => console.log('fin'))
}

function prepararRespuestaMultiOperacones(observable$) {
    observable$
        .pipe(mergeMap(respuesta => sumar(respuesta, 2)),
            mergeMap(respuesta => dividir(respuesta, 2)),
            map(resultado => {
                return formatearRespuesta(resultado)
            })
        )
        .subscribe(respuesta => console.log(respuesta),
            error => console.log('Error:', error),
            fin => console.log('fin'))
}


let continuar = true;

(async () => {

   do {

        const seleccionado = await prompts(preguntaFormaSolucion);
        if (seleccionado.value == 's') {
            console.log('Gracias por usar el sistema....');
        }
        switch (seleccionado.value) {
            case 'callBack':
                try {
                    const operacionCallBack = await prompts(preguntaOperacion);
                    console.log(typeof operacionCallBack.value);

                    switch (operacionCallBack.value) {
                        case 'suma':
                            try {
                                const ingresoNumeros = await prompts(preguntaIngresoNumero);
                                const RespuestaSuma = sumaCB(ingresoNumeros.numero1, ingresoNumeros.numero2);
                                console.log(RespuestaSuma);
                            } catch (e) {
                                console.log('Ocurrio un problema');
                            }
                            break;

                    }

                } catch (e) {
                    console.log('error', e);
                }
                break;


            case 'promesas':

                try {
                    const operacionPromesas = await prompts(preguntaOperacion);
                    switch (operacionPromesas.value) {
                        case 'suma':
                            try {
                                const ingresoNumeros = await prompts(preguntaIngresoNumero);
                                const resultadoSuma = await sumaPromesa(ingresoNumeros.numero1, ingresoNumeros.numero2);
                                console.log(resultadoSuma);

                            } catch (e) {
                                console.log(e);
                            }


                            break;
                    }

                } catch (e) {
                    console.log('error', e);
                }
                break;


            case 'observables':
                try {
                    const operacionObservable = await prompts(preguntaOperacion);
                    switch (operacionObservable.value) {
                        case 'suma':
                            const ingresoNumeros = await prompts(preguntaIngresoNumero);
                            const suma$ = from(sumaPromesa(ingresoNumeros.numero1, ingresoNumeros.numero2));
                            // suma$.subscribe(resultado=>{
                            //     console.log(resultado);
                            // })
                            prepararRespuesta(suma$);
                    }
                    //console.log(operacionObservable);
                } catch (e) {
                    console.log('error', e);
                }
                break;
        }
        const deseaContinuar = await prompts(desicionContinuar);
        continuar = deseaContinuar.continuar;
    }while(continuar === true);

})();
// const menu = async ()=>{
//     const respuesta = await prompts(preguntaFormaSolucion);
//     if(respuesta.value === 's'){
//         console.log('Gracias por usar el sistema....');
//     }else{
//         await operaciones(respuesta.value);
//     }
// }


// const operaciones = async (respuesta)=> {
//     console.log(respuesta)
//     switch (respuesta) {
//
//         case 'callBack':
//             try{
//                 const respuesta = await prompts(preguntaOperacion);
//                 console.log(respuesta);
//                 await menu();
//             }catch (e) {
//                 console.log('error', e);
//             }
//             break;
//
//
//         case 'promesas':
//             try{
//                 const respuesta = await prompts(preguntaOperacion);
//                 console.log(respuesta);
//                 await menu();
//             }catch (e) {
//                 console.log('error', e);
//             }
//             break;
//
//
//         case 'observables':
//             try{
//                 const respuesta = await prompts(preguntaOperacion);
//                 console.log(respuesta);
//                 await menu();
//             }catch (e) {
//                 console.log('error', e);
//             }
//             break;
//
//         case 'listar':
//             try{
//                 console.log(usuarios);
//                 await menu();
//             }catch(e){
//                 console.log('error', e);
//             }
//             break;
//     }
// }
// menu();